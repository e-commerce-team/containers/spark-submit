$SPARK_HOME/bin/spark-submit \
        --class Main \
        --master $SPARK_MASTER \
        --deploy-mode $DEPLOY_MODE \
        --properties-file properties.conf \
        --conf "spark.driver.extraJavaOptions=log4j.properties" \
        --conf "spark.executor.extraJavaOptions=log4j.properties" \
        --files $CONF_FILE_DIR/properties.conf,$LOG4J_DIR/log4j.properties \
         /*.jar
