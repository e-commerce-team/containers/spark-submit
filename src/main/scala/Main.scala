
import aggs.ECommerceStatistics
import com.mongodb.spark.MongoSpark
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.internal.Logging
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import util.{ECommerceConsts, ECommerceUtils}


object Main extends ECommerceConsts with Logging {

  def main(args: Array[String]) {

    val env = scala.util.Properties.envOrElse("ENVIRONMENT", "dev")
    
    val delay = scala.util.Properties.envOrElse("DELAY", "40").toInt
    val kafkaTopics = scala.util.Properties.envOrElse("KAFKA_TOPICS", "commerceRecords")
    val kafkaBootstrapServers = scala.util.Properties.envOrElse("KAFKA_BOOTSTRAP_SERVERS", "localhost:9092")

    val mongoBootstrapServers = scala.util.Properties.envOrElse("MONGO_BOOTSTRAP_SERVERS", "localhost:27015")
    val mongoURI = scala.util.Properties.envOrElse("MONGO_OUTPUT_URI", "mongodb://admin:pass@localhost/dev.commerceRecords")

    val kafkaConfPath = s"/kafka-$env.properties"

    val kafkaConfMap: collection.mutable.Map[String, String] = ECommerceUtils.loadConfig(kafkaConfPath)
    kafkaConfMap.put("bootstrap.servers", kafkaBootstrapServers)

    val spark = ECommerceUtils.startSparkSession()

    val ssc = new StreamingContext(spark.sparkContext, Seconds(delay))

    val preferredHosts = LocationStrategies.PreferConsistent
    val topics = kafkaTopics.split(',')
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      preferredHosts,
      ConsumerStrategies.Subscribe[String, String](topics, kafkaConfMap)
    )


    val aggFuncs: List[DataFrame => DataFrame] = List(ECommerceStatistics.totalBrandPriceByEvent)

    applyAggFuncs(stream, spark, aggFuncs)

    ssc.start()
    ssc.awaitTermination()
  }


  def applyAggFuncs(stream: InputDStream[ConsumerRecord[String, String]], spark: SparkSession, functions: List[DataFrame => DataFrame]): Unit = {
    log.info("Start processing")
    functions.foreach {
      aggFunction =>
        stream.foreachRDD { rdd =>
          if (!rdd.isEmpty) {
            val mappedRDD = rdd.map(_.value())
            val df = ECommerceUtils.rddToDF(mappedRDD, spark)
            val product_df = aggFunction(df)
            product_df.printSchema()
            product_df.show()
            //MongoSpark.save(product_df.write.mode("append"))
          } else {
            log.warn("RDD is empty")
          }
        }
    }
  }

}

