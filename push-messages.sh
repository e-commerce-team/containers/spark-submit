#!/usr/bin/env bash
docker run -d --name datasource1 --env PARALELLISM=1 --network ecommerce-net -v /home/xryash/local_repository/ecommerce-behavior-data-from-multi-category-store/data-source/repartitioned_data:/repartitioned_data ecommerce-data-source
sleep 8
docker kill datasource1 && docker rm datasource1