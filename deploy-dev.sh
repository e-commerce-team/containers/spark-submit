#!/usr/bin/env bash
export TMP_DIR=/tmp/spark-temp
export REBUILD=$1

if [ "$REBUILD" -eq  "1" ]; then
    sbt clean assembly
fi

if [ ! -d "TMP_DIR" ]; then
    mkdir -p TMP_DIR
fi

./spark/bin/spark-submit \
         --class Main \
         --master local[*] \
         --conf spark.local.dir=$TMP_DIR \
         target/scala-2.*/*.jar