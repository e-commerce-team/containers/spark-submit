val artifactVersion = sys.env.getOrElse("ART_VERSION", "1.0")
val organizationArtifact = sys.env.getOrElse("ORG_NAME", "test-org")
val artifactScalaVersion = sys.env.getOrElse("SCALA_VERSION", "2.11.8")
val artName = sys.env.getOrElse("ART_NAME", "test-art")
val sparkVersion = sys.env.getOrElse("SPARK_VERSION","2.4.5")
val sparkGroupId = "org.apache.spark"

version := artifactVersion
organization := organizationArtifact
scalaVersion := artifactScalaVersion


libraryDependencies ++= Seq(
  sparkGroupId %% "spark-sql" % sparkVersion % "provided",
  sparkGroupId %% "spark-core" % sparkVersion % "provided",
  sparkGroupId %% "spark-streaming" % sparkVersion % "provided",
  sparkGroupId %% "spark-streaming-kafka-0-10" % sparkVersion,
  "org.mongodb.spark" %% "mongo-spark-connector" % "2.4.1",
  "com.typesafe" % "config" % "1.3.2"
)

mainClass in assembly := Some("Main")

test in assembly := {}

assemblyJarName in assembly := s"$organizationArtifact.$artName-${artifactVersion}_scala-${artifactScalaVersion}_spark-$sparkVersion.jar"

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", "spark", "unused", "UnusedStubClass.class") => MergeStrategy.first
  case PathList("org", "mongodb", "spark", "unused", "UnusedStubClass.class") => MergeStrategy.first
  case x => (assemblyMergeStrategy in assembly).value(x)
}


