ARG BASE_IMAGE

FROM ${BASE_IMAGE}:latest

RUN cp *.jar log4j.properties properties.conf $SPARK_HOME/ \
        && chmod +x $SPARK_HOME/*.jar $SPARK_HOME/log4j.properties $SPARK_HOME/properties.conf

RUN cp entrypoint.sh /entrypoint.sh \
        && chmod +x /entrypoint.sh

ENTRYPOINT [ "bash", "entrypoint.sh"]
